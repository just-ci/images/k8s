FROM docker.io/alpine:edge

RUN apk add --no-cache --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing \
    kubectl helm helmfile git gettext

RUN helm plugin install https://github.com/databus23/helm-diff
